import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state:{
    username:'' || localStorage.getItem('username'),
    token:'' || localStorage.getItem('token'),
    id:'' || localStorage.getItem('id')
  },
  mutations:{
    setUser(state,obj){
      for(var i in obj){
        state[i]=obj[i]
        localStorage.setItem(i,obj[i])
      }
    },
    clearUser(state){
      localStorage.clear()
      state.username = ''
      state.token = ''
      state.id=''
    }
  },
  actions: {
  },
  modules: {
  }
})
