import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins/element.js'

import ajax from './assets/js/Ajax.js'
Vue.config.productionTip = false
Vue.prototype.ajax = ajax;

import 'element-ui/lib/theme-chalk/display.css';
import ElementUI from 'element-ui';

router.beforeEach((to, from, next) => {
  if(to.meta.login_pd){
    if(store.state.username && store.state.token){
      next()
    }else{
       ElementUI.MessageBox({       //element ui 弹窗
        title: '',
        message: '您还没登录,请登录'
      }).then(()=>{
        router.push('/login');
      })
    }
  }else{
    next()
  }
})
import animated from 'animate.css' // npm install animate.css --save安装，在引入
Vue.use(animated)

Vue.prototype.bus=new Vue();
import './assets/css/resize.css'
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
