import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)
const routes = [
  {
    path: '/',
    redirect:'/back/userlist'
  },
  {
    path:'/back',
    name:'back',
    component:()=>import('../views/back/back.vue'),
    meta:{
      login_pd:true
    },
    children:[
      {  //用户列表
        path:'userlist',
        name:'userlist',
        component:()=>import('../views/back/user/userlist.vue'),
        meta:{
          login_pd:true,
          title:['用户管理','用户列表'],
          noAuthorization:true,
          tt:'1-1'
        }
      },
      { //权限列表
        path:'permissions',
        name:'permissions',
        component:()=>import('../views/back/permission/permissions.vue'),
        meta:{
          login_pd:true,
          title:['权限管理','权限列表'],
          noAuthorization:true,
          tt:'2-2'
        }
      },
      { //角色列表
        path:'rolelist',
        name:'rolelist',
        component:()=>import('../views/back/permission/rolelist.vue'),
        meta:{
          login_pd:true,
          title:['权限管理','角色列表'],
          tt:'2-1',
          noAuthorization:true
        }
      },
      { //shop列表
        path:'shoplist',
        name:'shoplist',
        component:()=>import('../views/back/shop/shoplist.vue'),
        meta:{
          login_pd:true,
          title:['商品列表','商品列表'],
          tt:'3-1',
          noAuthorization:true
        }
      },
      { //商品分类
        path:'shopclass',
        name:'shopclass',
        component:()=>import('../views/back/shop/shopclass.vue'),
        meta:{
          login_pd:true,
          title:['商品列表','商品分类'],
          tt:'3-3',
          noAuthorization:true
        }
      },
      {
        path:'addshop',
        name:'addshop',
        component:()=>import('../views/back/shop/addshop.vue'),
        meta:{
          login_pd:true,
          title:['商品列表','商品添加'],
          tt:'3-1',
          noAuthorization:true
        }
      },
      { //分类参数
        path:'classparams',
        name:'classparams',
        component:()=>import('../views/back/shop/classparams.vue'),
        meta:{
          login_pd:true,
          title:['商品列表','分类参数'],
          tt:'3-2',
          noAuthorization:true
        }
      },
      { //订单列表
        path:'theordelist',
        name:'theordelist',
        component:()=>import('../views/back/theorde/theordelist.vue'),
        meta:{
          login_pd:true,
          title:['订单管理','订单列表'],
          tt:'4-1',
          noAuthorization:true
        }
      },
      { //数据管理
        path:'data',
        name:'data',
        component:()=>import('../views/back/data/data.vue'),
        meta:{
          login_pd:true,
          title:['数据管理','数据统计'],
          tt:'5-1',
          noAuthorization:true
        }
      }
    ]
  },
  {
    path:'/login',
    name:'login',
    component:()=>import('../views/login.vue'),
    noAuthorization:false
  }
]

const router = new VueRouter({
  mode: 'history',
  base: '/test/',
  routes,
})

export default router
