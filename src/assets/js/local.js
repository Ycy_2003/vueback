let obj ={
    set(name,data){
        window.localStorage.setItem(name,JSON.stringify(data))
    },
    get(name){
        return JSON.parse(window.localStorage.getItem(name))
    },
    clear(){
        window.localStorage.clear()
    }
}
export default obj;