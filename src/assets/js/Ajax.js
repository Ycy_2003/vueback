import axios from 'axios';
import {Message} from 'element-ui'
import vuex from '@/store/index.js';
import router from '@/router/index.js'

//添加请求拦截器
axios.interceptors.request.use(
    (config)=>{
        if(vuex.state.token){  //请求前判断 本地储存(vuex数据持久化)是否有token 如果有设置请求头
            config.headers = {Authorization: vuex.state.token}
        }
        if(!vuex.state.token){   //没有让其跳转至登录页
            router.replace({name:'login'}).catch(err=>{})
        }
        return config
    },
    (error)=>{
        return Promise.reject(error);
    }
)

axios.interceptors.response.use(
    (response)=>{
        if(response.data.meta.status == 400 && response.data.meta.msg == '无效token'){
            router.replace({name:'login'}).catch(err=>{})
        }
        return response
    },
    (error)=>{
        return Promise.reject(error)
    }
)

const baseURL = 'http://localhost:8889/api/private/v1/'
function fun(url,method='GET',params={},data={},headers){
    return new Promise((resolve,reject)=>{
        axios({
            baseURL,
            url,
            method,
            params,
            data,
            headers:headers,
        }).then(res=>{
            // 判断接口是否成功
            if(res.status >= 200 && res.status<300 || res.status===304){
                // 判断是否请求到数据
                if(res.data.meta.status >=200 && res.data.meta.status<300){
                    resolve(res)
                }
            }else{
                Message.error(res.statusText)
                reject(res)
            }
        }).catch(err=>{
            reject(err)
        })
    })
}
export default fun;
export const BURL = baseURL