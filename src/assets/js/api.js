import ajax from './Ajax.js'
let api = {
    async backList(){
        let res = await ajax('menus','get',{},{})
        return res;
    },
    async login(obj){
        let res = await ajax('login','POST',obj)
        return res
    },
    async getUser_list(obj){
        let res = await ajax('users','GET',obj,{})
        return res
    },
    async getCha(id){    //用户管理查询
        let res = await ajax(`users/${id}`,'get',{},{})
        return res
    },
    async setUser(id,obj){  //编辑角色信息提交
        let res = await ajax(`users/${id}`,'put',{},obj)
        return res
    },
    async setUserRole(id){    //编辑角色 列表
        let res = await ajax(`users/${id}`,'get',{},{})
        return res
    },
    async setUserRoleSub(id,obj){  //编辑角色提交
        let res = await ajax(`users/${id}/role`,'put',{},obj)
        return res
    },
    async delUser(id){  //删除角色
        let res = await ajax(`users/${id}`,'delete',{},{})
        return res
    },
    async userSwitch(id,ty){  //开关 状态
        let res = await ajax(`users/${id}/state/${ty}`,'put',{},{})
        return res
    },
    async addUser(obj){
        let res = await ajax(`users`,'post',{},obj)
        return res
    },

    // 权限列表
    async getPer_list(a1){
        let res = await ajax('rights/'+a1,'get',{},{})
        return res;
    },
    // 获取角色列表
    async getRoles(){
        let res = await ajax('roles','GET',{},{})
        return res
    },
    async perAddRole(data){   //添加角色
        let res = await ajax('roles','post',{},data)
        return res
    },
    async perDelRole(id){   //删除出角色
        let res = await ajax(`roles/${id}`,'delete',{},{})
        return res
    },
    async perSetRole(id,obj){   //编辑角色
        let res = await ajax(`roles/${id}`,'put',{},obj)
        return res
    },
    async delRolePer(roleId,rightId){  //删除单个权限
        let res = await ajax(`roles/${roleId}/rights/${rightId}`,'delete',{},{})
        return res
    },
    async getRole(id){
        let res = await ajax(`roles/${id}`,'get',{},{})
        return res
    },
    async setRolePer(id,list){   //编辑权限
        let res = await ajax(`roles/${id}/rights`,'POST',{},list)
        return res;
    },


    // 获取订单
    async getDDs(page,size){
        let res = await ajax('orders','get',{pagenum:page,pagesize:size},{})
        return res
    },
    // 编辑订单
    async setDDs(id,obj){
        let res = await ajax('orders/'+id,'put',{},obj)
        return res
    },

    // 商品列表
    async getShopList(page,size,query){  //获取商品列表
        let res = await ajax('goods','get',{pagenum:page,pagesize:size,query},{})
        return res;
    },
    async getShop(id){   //获取商品
        let res = await ajax(`goods/${id}`,'get',{},{})
        return res
    },
    async setShop(id,obj){  //编辑商品
        let res = await ajax(`goods/${id}`,'put',{},obj)
        return res
    },
    async delShop(id){  //删除商品
        let res = await ajax(`goods/${id}`,'delete',{},{})
        return res
    },
    // 添加商品,获取分类列表
    async getClass(arr,pagenum=1,pagesize=100){
        let res = await ajax(`categories`,'get',{type:arr,pagenum,pagesize},{})
        return res
    },
    async addShop(obj){   //添加商品
        let res = await ajax('goods','post',{},obj)
        return res
    },
    async addImgURL(file){
        let res = await ajax('upload','post',{},{file})
        return res
    },
    async getIdParams(id,attrid,sel){
        let res = await ajax(`categories/${id}/attributes/${attrid}`,'get',{attr_sel:sel},{})
        return res
    },


    
    async delClass(id){   //删除分类
        let res = await ajax('categories/'+id,'delete',{},{})
        return res
    },
    async setClass(id,name){  //编辑分类
        let res = await ajax('categories/'+id,'put',{},{cat_name:name})
        return res
    },
    async addClass(obj){   //添加分类
        let res = await ajax('categories','post',{},obj)
        return res
    },
    async getIdClass(id){   //根据id查询分类
        let res = await ajax('categories/'+id,'get',{},{})
        return res
    },
    
    async getParamsList(id,data){
        let res = await ajax(`categories/${id}/attributes`,'get',{sel:data},{})
        return res
    },
    async addParams(id,ids,obj){   //添加tag标签,设置tag,编辑
        console.log(obj)
        let res = await ajax(`categories/${id}/attributes/${ids}`,'put',{},obj)
        return res
    },
    async delParams(id,attrid){   //删除参数
        let res = await ajax(`categories/${id}/attributes/${attrid}`,'delete',{},{})
        return res
    },
    async addOMParams(id,obj){  //添加动态参数/或静态参数
        let res = await ajax(`categories/${id}/attributes`,'post',{},obj)
        return res
    },


    // 折线图
    async getData(){
        let res = await ajax('reports/type/1','get',{},{})
        return res
    }

}
export {api};
